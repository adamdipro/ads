package docker.api.exceptions;

public class ContainerException extends Exception{
public ContainerException(){
	super();
}
public ContainerException(String message){
	super(message);
}

}

package docker.api.exceptions;

public class ServerErrorException extends Exception{
public ServerErrorException(){
	super();
}
public ServerErrorException(String strMessage){
	super(strMessage);
}

}

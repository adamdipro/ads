package docker.api.exceptions;

public class UnknownErrorException extends Exception{
public UnknownErrorException(){
	super();
}
public UnknownErrorException(String message){
	super(message);
}

}

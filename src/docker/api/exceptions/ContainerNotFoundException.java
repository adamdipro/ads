package docker.api.exceptions;

public class ContainerNotFoundException extends Exception{
public ContainerNotFoundException(){
	super();
}
public ContainerNotFoundException(String strContainer){
	super("Container [" + strContainer + "] not found.");
}

}

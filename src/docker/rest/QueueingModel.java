package docker.rest;



public class QueueingModel{
	double arrivalrate;
	double demand;
	double actualUtilization;
	public void setModelParameters(double ar, double demand){
		arrivalrate = ar; //Throughput gathered from JMeter
		this.demand = demand;
		
		
	}
	public double runModel(int controllerOutput){
		double Utilization = 0;
		Utilization = (arrivalrate * demand)/controllerOutput;
		System.out.println("Model Utilization: "+ Utilization );
		return Utilization;
		
	}
}
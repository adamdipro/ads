//ADAM: this is only used for 'DocRestClient.java' NOT for Yar's code, which is PerformanceUtilizationDisplay.java'
package docker.rest;

public class PidController {
	
	//TODO: remember that there are variables that exist outside of the scope of the computation..
	long lastTime =0;
	double input, output, setpoint;
	double errSum =0;
	double lastErr =0;
	double kp,ki,kd;
	
	void compute()
	{
		//How long since we last calculated
		long now = (System.currentTimeMillis()/1000);
		double timeChange = (double)(now - lastTime);
	//	System.out.println("The value of timeChange is: " + timeChange);
	//	System.out.println("The value of now is is: " + now);
		//Compute all the working error variables
//		double error =  setpoint - input; 
		double error = input - setpoint;     // NOTE: I switched around to subtract the setpoint from the input.. not sure if this is correct   
		errSum = errSum + (error * timeChange); 
		 
		// This is the derivative portion... Refer to triangle Dariush drew where a= error-last error, and b = time.. this has to do with cosine
		   double dErr = (error - lastErr) / timeChange;
		   
		   //Compute PID Output
		   output = kp * error + ki * errSum + kd * dErr;
		  
		   //Remember some variables for next time
		   lastErr = error;
		   lastTime = now;
		  
	}
	// This is what comes in from the user... if we use Zigler-Nichols, we just pass in specific values... for manual tuning, we try our own.
	  void setTunings(double Kp, double Ki, double Kd)
	   {
	      kp = Kp;
	      ki = Ki;
	      kd = Kd;
	   }
	
}
/**
 * 							Pseudo code for the main method that will run the controller 
 * main()
{
   SetTunings(1,0,0);
   lastErr=0;
   errSum=0;
   lastTime=now;
   do {

      read Input;
      Compute();
      Apply Output; // this would take the output, send it to some other method that will look at that number and determine if containers should be added or removed
      //see the if statements from the sheet that Dariush wrote... 
      // remember that if the output is positive 
      wait 1 sec;
   } while(forever);
}
 */


/** 			----- 	SAMPLE CODE -----
 *working variables
unsigned long lastTime; //+
double Input, Output, Setpoint; //+ 
double errSum, lastErr; //+
double kp, ki, kd; //+
void Compute() //+
{ //+
   /*How long since we last calculated
   unsigned long now = millis(); //+
   double timeChange = (double)(now - lastTime); //+
  
   /*Compute all the working error variables //+
   double error = Setpoint - Input; //+
   errSum += (error * timeChange); //+
   
// This is the derivative portion... Refer to triangle Dariush drew where a= error-last error, and b = time.. this has to do with cosine
  
   double dErr = (error - lastErr) / timeChange; //*
  
   /*Compute PID Output//+
   Output = kp * error + ki * errSum + kd * dErr;//+
  
   /*Remember some variables for next time//+
   lastErr = error;//+
   lastTime = now;//+
}//+


// This is what comes in from the user... if we use Zigler-Nichols, we just pass in specific values... for manual tuning, we try our own.
void SetTunings(double Kp, double Ki, double Kd)
{
   kp = Kp;
   ki = Ki;
   kd = Kd;
}



quesitons

How do we store lastErr? do we run this multiple times and pull from a variable that is stored in the program
**/ 
 

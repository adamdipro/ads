package docker.rest;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import docker.model.Container;
import docker.model.ContainerStats;
import docker.api.exceptions.ContainerException;
import docker.api.exceptions.ContainerNotFoundException;
import docker.api.exceptions.ServerErrorException;
import docker.api.exceptions.UnknownErrorException;

import java.util.Collections;

public class PerformanceUtilizationDisplay {
	static List<String> nameList = new ArrayList<String>();
	static List<Integer> nameNum = new ArrayList<Integer>();
	
	public static final void main(String[] args) throws IOException, ParseException, ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException, InterruptedException {
		String DockerListURL = "http://52.23.168.200:4000/containers/json";
		
		HttpURLConnection request = null;
		HttpURLConnection cpuRequest = null;
		try {
			URL url = new URL(DockerListURL);
			
			
			try {
				request = (HttpURLConnection) url.openConnection();
				request.connect();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
  
		JSONParser jp = new JSONParser(); //from gson
	    JSONArray dockerArray = (JSONArray) jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element		
	    String nfNames; //Not Formatted Names
	    List<Integer> allCpuUsage = new ArrayList<Integer>();
	    
	   
	    
	    for (int i = 0; i < dockerArray.size(); i++){
		   JSONObject dockerObject = (JSONObject) dockerArray.get(i);
		   nfNames = dockerObject.get("Names").toString();
/** ADAM: trying to output the unformatted name, in order to determine what to substring to get in aws **/ System.out.println("The unformatted name is: " + nfNames);		
		   //String fName = nfNames.substring( 21 , nfNames.indexOf("\"]") ); //formatted name

//******** Adam: Added the following to try and get parse out the proper web worker names
			String str = "web";
			int location = 0;
			if (nfNames.toLowerCase().contains(str))
				location = nfNames.indexOf('w');

			String fName = nfNames.substring(location , nfNames.indexOf("\"]") ); //formatted name
//**********
			System.out.println("The formatted name is: " + fName);
		 // if(!(fName.equals("mysql") || fName.equals("load-balancer.dataop"))){ // ADAM: due to the change above,this will no longer work, so instead I got it to check if the name contains 'web'
			if(fName.contains(str)){
			   nameList.add(fName);
		   

			   
		   //CPU UTILIZATION
		
		   String DockerCPUURL = "http://52.23.168.200:4000/containers/"+ fName +"/stats?stream=false";
		  
			try {
				URL url = new URL(DockerCPUURL);
				cpuRequest = (HttpURLConnection) url.openConnection();
				cpuRequest.connect();
			} catch (IOException e) {
				e.printStackTrace();
			}/* OLD CPU CALCULATION
			JSONObject cpuObject = (JSONObject) jp.parse(new InputStreamReader((InputStream) cpuRequest.getContent()));
			System.out.println(cpuObject.get("cpu_stats"));
			System.out.println(cpuObject.get("precpu_stats"));
			
			JSONObject cpu_stats = (JSONObject) cpuObject.get("cpu_stats");
			JSONObject cpu_usage = (JSONObject) cpu_stats.get("cpu_usage");
			
			JSONObject precpu_stats = (JSONObject) cpuObject.get("precpu_stats");
			JSONObject precpu_usage = (JSONObject) precpu_stats.get("cpu_usage");
			
			//calculation
			double cpuDelta = Double.parseDouble(cpu_usage.get("total_usage").toString()) - Double.parseDouble(precpu_usage.get("total_usage").toString());
			double systemDelta = Double.parseDouble(cpu_stats.get("system_cpu_usage").toString()) - Double.parseDouble(precpu_stats.get("system_cpu_usage").toString());
			System.out.println("CPU USAGE: " + cpuDelta + " / " + systemDelta);
			double cpuUsageCalculation = (cpuDelta / systemDelta) * 100;
			System.out.println( cpuUsageCalculation );
		
			*/
			
		   }
		   
	   }
	    
	    
	    
	  //ADAM: used the below line for testing: try to bring out the content of nameList
//	    System.out.println(nameList);
	

	    	    
	    if (nameList.isEmpty()){
	    	nameNum.add(0);
	    }
	    else{
		    for (int i = 0; i < nameList.size(); i++){
		    	System.out.println(nameList.get(i));
		    	nameNum.add(Integer.parseInt(nameList.get(i).replaceAll("[\\D]", "")));
		    	//System.out.println());  Integer.parseInt(nameList.get(i).replaceAll("[\\D]", "")
		    }
	    }
	    System.out.println("The current max nameNum is: " + Collections.max(nameNum));
	    
	    
	    
	  // ADAM: I will comment out the below for now, just to see if the above gives me the correct info from AWS
	 
	    
	    DocRestClient Client = new DocRestClient("52.23.168.200:4000");
	   
//	    Client.removeMultiple(7);
	    
	    PIDController pidController = new PIDController();
	    pidController.Setpoint(20);
	    pidController.tuning(0.05, 0.001, 0.001);
	    //pidController.tuning(0.50, 0, 0);
	    // ki 0.000001
	    int containerModifier;
	 
	    //set up the print writer to output to a file
	    PrintWriter writer;
		writer = new PrintWriter("AWS-Controller-Results"+ System.currentTimeMillis() +".csv", "UTF-8");
		writer.println("Time,Avg Utilization (100.00),# of Containers");

	    
	   //while(true){
		 for(int i = 0; i<9; i++){  
		   writer.println((System.currentTimeMillis()/1000) +"," + Client.AvgCpuUsage() + "," + nameNum.size());
		   
		   containerModifier = (int)pidController.PID(Client.AvgCpuUsage());
		   System.out.println(containerModifier);
	    	if (containerModifier > 0){
	    		Client.addMultiple(containerModifier);
	    		TimeUnit.SECONDS.sleep(10);
	    	}else if (containerModifier < 0 && nameNum.size() > 1){
	    		Client.removeMultiple(Math.abs(containerModifier));
	    		TimeUnit.SECONDS.sleep(10);
	    	}else{
	    		TimeUnit.SECONDS.sleep(20);
	    	}
	    	
	    	
	    }
	    //Client.addMultiple(1);
	    
	   writer.close();
	

	 
		  //ADAM: *******I am just adding the following in order to test and confirm that addMultiple and removeMultiple work:
//	    DocRestClient Client = new DocRestClient("52.23.168.200:4000");
	    
//	 Client.addMultiple(5);   
//	 Client.removeMultiple(22);
	  ///***********  	    
	    

	}
	

// end of main method.
	
	
////////////////////////////////////////////////////////////
/**
	public static class DocRestClient
	{
	    private String m_strEndPoint = "http://52.23.168.200:4000/v1.22";
	    private JsonParser theJsonParser = new JsonParser();
	    Gson gson = new Gson();
	    Map<String, String> mapDocTemplate = new HashMap<>();

	    public DocRestClient(String strHost)
	    {
	        this.m_strEndPoint = "http://" + strHost;
	    }

	    private String GetDocTemplate(String strTemplate)
	    {
	        if (this.mapDocTemplate.containsKey(strTemplate) == false)
	        {
	            // load template
	            StringBuilder stringBuilder = new StringBuilder();
	            FileReader fileReader = null;
	            BufferedReader bufferedReader = null;

	            try
	            {
	                fileReader = new FileReader("./docker-container-templates/" + strTemplate);
	                bufferedReader = new BufferedReader(fileReader);
	                char[] buf = new char[23];
	                int numRead=0;

	                while((numRead = bufferedReader.read(buf)) != -1)
	                {
	                    String readData = String.valueOf(buf, 0, numRead);
	                    stringBuilder.append(readData);
	                }
	                bufferedReader.close();
	                fileReader.close();
	            }
	            catch (Exception ex) { ex.printStackTrace(); }

	            this.mapDocTemplate.put(strTemplate, stringBuilder.toString());
	        }
	        return this.mapDocTemplate.get(strTemplate);
	    }

	    public String CreateContainer(String strName, String strTemplate) throws ContainerNotFoundException, ServerErrorException, ContainerException, UnknownErrorException
	    {
	        String strPayload = this.GetDocTemplate(strTemplate);
	        RestCallResult restResult = this.RestCallPost(this.m_strEndPoint + "/containers/create?name=" + strName, strPayload);

	        if (restResult.responseCode == 201)
	        {
	            JsonObject jObj = theJsonParser.parse(restResult.responseBody).getAsJsonObject();
	            return jObj.get("Id").getAsString();
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strName);
	        }
	        else if (restResult.responseCode == 406)
	        {
	            throw new ContainerException(strName);
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while creating container [" + strName + "].");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while creating container [" + strName + "]."); 
	        }
	    }

	    public void StartContainer(String strName) throws ContainerNotFoundException, ServerErrorException, UnknownErrorException
	    {
	        RestCallResult restResult = this.RestCallPost(this.m_strEndPoint + "/containers/" + strName + "/start", null);

	        if (restResult.responseCode == 204 || restResult.responseCode == 304)
	        {
	            return;
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strName);
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while creating container [" + strName + "].");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while creating container [" + strName + "]."); 
	        }
	    }

	    public String RunContainer(String strName, String strTemplate) throws ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException
	    {
	        // starting a container could fail if the VM is very new. If we retry, there is a chance that the creating actually succeeds.
	        String strId = null;
	        int attempt = 0;
	        int attemptsMax = 5;
	        long timeBetweenAttempts = 1000;
	        while (attempt < attemptsMax)
	        {
	            try
	            {
	                strId = this.CreateContainer(strName, strTemplate);
	                break;
	            }
	            catch (ContainerNotFoundException | ContainerException | UnknownErrorException | ServerErrorException e)
	            {
	                ++attempt;
	                if (attempt >= attemptsMax)
	                {
	                    // done with the attempt
	                    throw e;
	                }
	                try
	                {
	                    // wait a little before a new attempt.
	                    Thread.sleep(timeBetweenAttempts);
	                }
	                catch (InterruptedException e1) { }
	            }
	        }

	        // if we reached this point, then the creation of the container succeeded.
	        this.StartContainer(strName);
	        return strId;
	    }

	    public void RestartContainer(String strName) throws ContainerNotFoundException, ServerErrorException, UnknownErrorException
	    {
	        RestCallResult restResult = this.RestCallPost(this.m_strEndPoint + "/containers/" + strName + "/restart", null);
	        if (restResult.responseCode == 204)
	        {
	            return;
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strName);
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while restarting container [" + strName + "].");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while restarting container [" + strName + "]."); 
	        }
	    }

	    public void RemoveContainer(String strName) throws ContainerException, ContainerNotFoundException, ServerErrorException, UnknownErrorException
	    {
	        RestCallResult restResult = this.RestCallDelete(this.m_strEndPoint + "/containers/" + strName + "?v=1&force=1");

	        if (restResult.responseCode == 204)
	        {
	            return;
	        }
	        else if (restResult.responseCode == 400)
	        {
	            throw new ContainerException(strName);
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strName);
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while creating container [" + strName + "].");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while creating container [" + strName + "]."); 
	        }
	    }

	    public ContainerStats GetStats(String strContainer) throws ServerErrorException, ContainerNotFoundException, UnknownErrorException
	    {
	        RestCallResult restResult = RestCallGet(this.m_strEndPoint + "/containers/" + strContainer + "/stats?stream=false");
	        if (restResult.responseCode == 200)
	        {
	            return gson.fromJson(restResult.responseBody, ContainerStats.class);
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strContainer);
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while getting");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while getting statistics for container [" + strContainer + "]."); 
	        }
	    }

	    public String ExecCreate(String strContainerName, String strScript) throws ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException
	    {
	        String strPayload = ""
	                + "{"
	                + "\"AttachStdin\": false,"
	                + "\"AttachStdout\": true,"
	                + "\"AttachStderr\": true,"
	                //+ "\"DetachKeys\": \"ctrl-p,ctrl-q\""
	                + "\"Tty\": false,"
	                + "\"Cmd\": [\"/bin/sh\", \"-c\", \""
	                + strScript
	                + "\"]"
	                + "}";

	        RestCallResult restResult = this.RestCallPost(this.m_strEndPoint + "/containers/" + strContainerName + "/exec", strPayload);

	        if (restResult.responseCode == 201)
	        {
	            JsonObject jObj = theJsonParser.parse(restResult.responseBody).getAsJsonObject();
	            return jObj.get("Id").getAsString();
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strContainerName);
	        }
	        else if (restResult.responseCode == 409)
	        {
	            throw new ContainerException("Container [" + strContainerName + "] is paused.");
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while creating exec in container [" + strContainerName + "].");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while creating exec [" + strContainerName + "]."); 
	        }

	    }

	    public String ExecStart(String strId) throws ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException
	    {
	        String strPayload = ""
	                + "{"
	                + "\"Detach\": false,"
	                + "\"Tty\": false"
	                + "}";

	        RestCallResult restResult = this.RestCallPost(this.m_strEndPoint + "/exec/" + strId + "/start", strPayload);
	        if (restResult.responseCode == 200)
	        {
	            return restResult.responseBody;
	        }
	        else if (restResult.responseCode == 404)
	        {
	            throw new ContainerNotFoundException(strId);
	        }
	        else if (restResult.responseCode == 409)
	        {
	            throw new ContainerException("Container is paused.");
	        }
	        else if (restResult.responseCode == 500)
	        {
	            throw new ServerErrorException("Server error while executing the commnad [" + strId + "].");
	        }
	        else
	        {
	            // unknown response code
	            throw new UnknownErrorException("Unknown response code [" + restResult.responseCode + "] from server while executing command [" + strId + "]."); 
	        }

	    }

	    public String Exec(String strContainerName, String strScript) throws ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException
	    {
	        String strId = null;
	        strId = this.ExecCreate(strContainerName, strScript);
	        return this.ExecStart(strId);
	    }

	    private RestCallResult RestCallDelete(String strRestUrl)
	    {
	        RestCallResult result = new RestCallResult();

	        try
	        {
	            URL url = new URL(strRestUrl);
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            conn.setRequestMethod("DELETE");
	            conn.setRequestProperty("Content-Type", "application/json");

	            result.responseCode = conn.getResponseCode();
	            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String output;
	            while ((output = br.readLine()) != null)
	            {
	                result.responseBody += output;
	            }

	            conn.disconnect();
	        }
	        catch (Exception e) {  }

	        return result;
	    }

	    private RestCallResult RestCallPost(String strRestUrl, String strPayload)
	    {
	        RestCallResult result = new RestCallResult();

	        try
	        {
	            URL url = new URL(strRestUrl);
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            conn.setRequestMethod("POST");
	            conn.setRequestProperty("Content-Type", "application/json");

	            if (strPayload != null)
	            {
	                OutputStream os = conn.getOutputStream();
	                os.write(strPayload.getBytes());
	                os.flush();
	            }

	            result.responseCode = conn.getResponseCode();

	            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

	            String output;
	            while ((output = br.readLine()) != null)
	            {
	                result.responseBody += output;
	            }

	            conn.disconnect();
	        }
	        catch (Exception e) {  }

	        return result;
	    }

	    private RestCallResult RestCallGet(String strRestUrl)
	    {
	        RestCallResult result = new RestCallResult();

	        try
	        {
	            URL url = new URL(strRestUrl);

	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setRequestMethod("GET");
	            conn.setRequestProperty("Accept", "application/json");

	            result.responseCode = conn.getResponseCode();

	            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	            String output;
	            while ((output = br.readLine()) != null)
	            {
	                result.responseBody += output;
	            }

	            conn.disconnect();
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return result;
	    }

	    private class RestCallResult
	    {
	        int responseCode = 0;
	        String responseBody = "";
	    }

	    public Container[] GetContainers()
	    {
	        RestCallResult restResult = RestCallGet(this.m_strEndPoint + "/containers/json");

	        if (restResult.responseCode == 200)
	        {
	            return gson.fromJson(restResult.responseBody, Container[].class);
	        }
	        return new Container[0];
	    }
	
	    
	    public void addMultiple(int add ) throws ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException{
	    	try {
	    		for (int i = 0; i < add; i++){
	    			if(nameNum.size() < 35){ // changed this from 8 to 20
		    			this.RunContainer( "web-worker-0"+ (Collections.max(nameNum) + 1) +".dataop", "dataop.webapp.doctpl");
		    			TimeUnit.SECONDS.sleep(6);
		    			this.Exec("load-balancer.dataop", "/lb-add-worker.sh web-worker-0"+ (Collections.max(nameNum) + 1) + ".dataop:9201");
		    			nameList.add("web-worker-0"+ (Collections.max(nameNum) + 1)+".dataop");
		    			nameNum.add((Collections.max(nameNum) + 1));
		    			System.out.println("Container: "+ nameList.get(nameList.size() - 1) +" has been added to Load Balancer");
	    			}
	    		}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	    public void removeMultiple(int remove) throws ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException{
	    	try {
	    		for (int i = 0; i < remove; i++){
	    			if(nameNum.size() > 1){
		    			this.Exec("load-balancer.dataop", "/lb-rm-worker.sh web-worker-0" + Collections.max(nameNum) + ".dataop");
		    			TimeUnit.SECONDS.sleep(6);
		    			this.RemoveContainer(("web-worker-0" + Collections.max(nameNum) + ".dataop"));
		    			System.out.println("Container: " +nameList.get(0) +" has been removed to Load Balancer");
		    			nameList.remove(nameNum.indexOf(Collections.max(nameNum)));
		    			nameNum.remove(nameNum.indexOf(Collections.max(nameNum)));
	    			}
	    			
	    		}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	    
	    public static double CpuUsage(ContainerStats stats)
	    {
	    	long cpuSystemUsage = stats.cpuStatsCurrent.cpuUsageSystem;
	    	long cpuUsage = stats.cpuStatsCurrent.cpuUsage.usageTotal;
	        double cpuUtilization = 100.0 * (cpuUsage       - stats.cpuStatsPrevious.cpuUsage.usageTotal)
	                                      / (cpuSystemUsage - stats.cpuStatsPrevious.cpuUsageSystem);
	        //System.out.println("CPU Utilization for container [dataop.web-worker-01] is [" + cpuUtilization + "]."); // ADAM NOTE: there was an error here, but I just moved the period within the bracket.
	        
	 	   
	 	   	return cpuUtilization;
	    }
	    
	    public double AverageCpuUsage(List<String> nameList) throws ServerErrorException, ContainerNotFoundException, UnknownErrorException
	    {
	    	List<Double> total = new ArrayList();
	 	   
	 	   for(int i =0; i<nameList.size(); i++){
	 		   ContainerStats currentStats = this.GetStats(nameList.get(i).toString());
	 		   double currentCpuUsage = CpuUsage(currentStats);
	 		  total.add(currentCpuUsage);
	 	   }
	 	   double sum = 0; 
	       double averageUsage = 0;
	       for(int i=0;i<total.size(); i++){
	        	sum= sum + (double)total.get(i);	
	        }
	      
	        averageUsage = sum/total.size();
	        
	        System.out.println("Total Average Utilization: " + averageUsage );
	        return averageUsage;
	 	   
	    }
	    
	}
	
	**/
	
	public static class PIDController{
		double kp, ki, kd;
		double setpoint;
		double etdt = 0;
		long lastTime = System.currentTimeMillis()/1000;
		double lastError = 0;
		int time = 0;
		
		public void Setpoint(double sp){
			setpoint = sp;
		}
		public void tuning(double KP, double KI, double KD ){
			kp = KP;
			ki = KI;
			kd = KD;
		}
		
		public double PID(double input){
			double error = input - setpoint; // Calculating Error e(t)
			
			// Proportional Mode
			double porpMode = kp * error;
			/////////////////////
			
			//Integral Mode
			long currentTime = System.currentTimeMillis()/1000;
			
			

			
			double tChange = currentTime - lastTime; //Change in Time
		
			etdt = etdt + (error * tChange); //integral e(t)dt
			System.out.println("time Change: "+ tChange + " , error: " + error + ", errorsum: " + etdt);
			lastTime = currentTime; // Store current time into Last time for next iteration
			
			
			
			double integMode = ki * etdt;
			
			//Derivative Mode
			double detdt = (error - lastError)/tChange;
			
			lastError = error; //store Error for next iteration
			
			double derivatMode = kd * detdt; 
			
			double PID = porpMode + integMode + derivatMode; //Sum for PID
			
			try {
				Files.write(Paths.get("graphingresults.txt"), ((int)currentTime+" , "+input + ", " ).getBytes(), StandardOpenOption.APPEND);
				time = time + 1;
			}catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
			
			
			return PID;
		}
		
	}
	
}

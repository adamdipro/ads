package docker.rest;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import docker.model.Container;
import docker.model.ContainerStats;
import docker.api.exceptions.ContainerException;
import docker.api.exceptions.ContainerNotFoundException;
import docker.api.exceptions.ServerErrorException;
import docker.api.exceptions.UnknownErrorException;

import java.util.Collections;

public class QNMDisplay {
	static List<String> nameList = new ArrayList<String>();
	static List<Integer> nameNum = new ArrayList<Integer>();
	
	public static final void main(String[] args) throws IOException, ParseException, ContainerNotFoundException, ContainerException, ServerErrorException, UnknownErrorException, InterruptedException {
		String DockerListURL = "http://52.23.168.200:4000/containers/json";
		
		HttpURLConnection request = null;
		HttpURLConnection cpuRequest = null;
		try {
			URL url = new URL(DockerListURL);
			
			
			try {
				request = (HttpURLConnection) url.openConnection();
				request.connect();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
  
		JSONParser jp = new JSONParser(); //from gson
	    JSONArray dockerArray = (JSONArray) jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element		
	    String nfNames; //Not Formatted Names
	    List<Integer> allCpuUsage = new ArrayList<Integer>();
	    
	   
	    
	    for (int i = 0; i < dockerArray.size(); i++){
		   JSONObject dockerObject = (JSONObject) dockerArray.get(i);
		   nfNames = dockerObject.get("Names").toString();
/** ADAM: trying to output the unformatted name, in order to determine what to substring to get in aws **/ System.out.println("The unformatted name is: " + nfNames);		
		   //String fName = nfNames.substring( 21 , nfNames.indexOf("\"]") ); //formatted name

//******** Adam: Added the following to try and get parse out the proper web worker names
			String str = "web";
			int location = 0;
			if (nfNames.toLowerCase().contains(str))
				location = nfNames.indexOf('w');

			String fName = nfNames.substring(location , nfNames.indexOf("\"]") ); //formatted name
//**********
			System.out.println("The formatted name is: " + fName);
		 // if(!(fName.equals("mysql") || fName.equals("load-balancer.dataop"))){ // ADAM: due to the change above,this will no longer work, so instead I got it to check if the name contains 'web'
			if(fName.contains(str)){
			   nameList.add(fName);
		   

			   
		   //CPU UTILIZATION
		
		   String DockerCPUURL = "http://52.23.168.200:4000/containers/"+ fName +"/stats?stream=false";
		  
			try {
				URL url = new URL(DockerCPUURL);
				cpuRequest = (HttpURLConnection) url.openConnection();
				cpuRequest.connect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		   }
		   
	   }
	    
	    
	    
	  //ADAM: used the below line for testing: try to bring out the content of nameList
//	    System.out.println(nameList);
	

	    	    
	    if (nameList.isEmpty()){
	    	nameNum.add(0);
	    }
	    else{
		    for (int i = 0; i < nameList.size(); i++){
		    	System.out.println(nameList.get(i));
		    	nameNum.add(Integer.parseInt(nameList.get(i).replaceAll("[\\D]", "")));
		    	//System.out.println());  Integer.parseInt(nameList.get(i).replaceAll("[\\D]", "")
		    }
	    }
	    System.out.println("The current max nameNum is: " + Collections.max(nameNum));
	    
	    
	    
	  // ADAM: I will comment out the below for now, just to see if the above gives me the correct info from AWS
	  
	    
	  	

    DocRestClient theClient = new DocRestClient("52.23.168.200:4000");
   	 searchController search = new searchController();
   	 search.setValues(20);
   	 double arrivalrate = 100; //100 seconds
   	 double demand = 1.0; // 701 ms (to seconds)
   	 System.out.println("Containers: " + search.returnContainerAmount(arrivalrate, demand, 99));  //Third parameter would be actual Utilization. We might be able to use it for real time adaption?
   	 int actualContainers = dockerArray.size() - 1 ;
   	
   	 //set up the print writer to output to a file
	    PrintWriter writer;
		writer = new PrintWriter("AWS-QNM-Controller-Results"+ System.currentTimeMillis() +".csv", "UTF-8");
		writer.println("Time,Avg Utilization (100.00),# of Containers");

   	 
   	 
   	 for(int i = 0; i<9;i++){
   		double avgcpu = theClient.AvgCpuUsage();
	    	System.out.println("The average utilization is: " + avgcpu);
   		 
   		 int modelContainers = search.returnContainerAmount(arrivalrate, demand, avgcpu);
   		 
 		   int containerModifier = modelContainers - actualContainers;
 		  
 		   writer.println((System.currentTimeMillis()/1000) +"," + theClient.AvgCpuUsage() + "," + nameNum.size());
 		   
 		   System.out.println("The value for containerModifier - the number of containers to add/remove is: " + containerModifier);
 	    	if (containerModifier > 0){
 	    		theClient.addMultiple(containerModifier);
 	    		actualContainers = actualContainers + containerModifier;
 	    		
 	    		TimeUnit.SECONDS.sleep(4);
 	    	}else if (containerModifier < 0 && nameNum.size() > 1){
 	    		theClient.removeMultiple(Math.abs(containerModifier));
 	    		actualContainers = actualContainers - containerModifier;
 	    		TimeUnit.SECONDS.sleep(4);
 	    	}else{
 	    		TimeUnit.SECONDS.sleep(10);
 	    	}
	    
 	    	//double avgcpu = theClient.AvgCpuUsage();
 	    	System.out.println("The average utilization is: " + avgcpu);
   	 		}
   	 
   	 	writer.close();
		}
	}

// end of main method.
	
	
////////////////////////////////////////////////////////////

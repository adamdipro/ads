package docker.model;

import com.google.gson.annotations.SerializedName;

public class ContainerStats {
    @SerializedName("cpu_stats")
    public CpuStats cpuStatsCurrent;
    @SerializedName("precpu_stats")
    public CpuStats cpuStatsPrevious;
    @SerializedName("memory_stats")
    public MemoryStats memory;

    public static class CpuStats
    {
        @SerializedName("system_cpu_usage")
        public long cpuUsageSystem;

        @SerializedName("cpu_usage")
        public CpuUsageContainer cpuUsage;

        @SerializedName("throttling_data")
        public CpuThrottling cpuThrottling;
    }

    public static class CpuUsageContainer
    {
        @SerializedName("total_usage")
        public long usageTotal;
        @SerializedName("usage_in_kernelmode")
        public long usageKernel;
        @SerializedName("usage_in_usermode")
        public long usageUser;
        @SerializedName("percpu_usage")
        public long[] usagePerCpu;
    }

    public static class CpuThrottling
    {
        @SerializedName("periods")
        public long throttlingPeriods;
        @SerializedName("throttled_periods")
        public long throttledPeriods;
        @SerializedName("throttled_time")
        public long throttledTime;
    }

    public static class MemoryStats
    {
        @SerializedName("usage")
        public long usage;
        @SerializedName("max_usage")
        public long max;
        @SerializedName("limit")
        public long limit;
    }
}
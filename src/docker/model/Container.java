package docker.model;

import com.google.gson.annotations.SerializedName;

public class Container {
    @SerializedName("Id")
    public String strId;
    
    @SerializedName("Names")
    public String[] strNames;
    
    @SerializedName("Image")
    public String strImage;
    
    @SerializedName("Command")
    public String strCommand;
    
    @SerializedName("Created")
    public long dateCreated;
}